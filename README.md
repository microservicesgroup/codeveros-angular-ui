# Codeveros UI

Core Codeveros UI angular application. Includes auth, catalog, and user angular libraries. These libraries need to be separately built prior to building the overall application

## Installing dependencies:

Run `npm install` to download the dependencies specified in the `package.json` file. This will update or create the `package-lock.json` file.

Run `npm ci` to install the dependencies specified in the `package-lock.json` file. This will fail if `package-lock.json` does not exist. This command is mainly used as part of
the CI pipeline and building the docker image, to ensure the same dependencies used locally by the committer are the ones used by Jenkins.


## Building the application:

Run `npm run build.libraries` to build all of the libraries at once. This npm script is specified in the `package.json` file. You may instead choose to individually build each
library, as an example, if only one library is being actively developed. To build each library, run the following commands 

`ng build auth`
`ng build user`
`ng build catalog`

You may choose to have the library automatically rebuild on every file modification. Use the `--watch` argument to have the libraries rebuilt as changes are made
(e.g. `ng build auth --watch`)

Once all libraries have been built, run `ng build` to build the overall application. For building for different configurations -- such as production -- you can specify the
configuration as build argument `ng build -c production`.

To build the production files from scratch, you may simply run `npm run build.production`. This will build all the libraries and the application.

The built artifacts will be stored in the `dist/` directory.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

This will automatically run `ng build`, but it will not automatically build the libraries. You will still need to build those prior to starting the dev server.

To specify a proxy file allowing you configure the endpoints in which to route API requests, use the `--proxy-config [path/to/proxy.file]` when starting the dev server.
An example of this file is `proxy.default.json` located in the top-level directory. (e.g `ng serve --proxy-config proxy.json`). This allows you to locally run the various services, and
configure the ports as needs dictate. By default, the API requests are sent to the same host and port that is serving the UI files.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Running code style checks

Run `ng lint` to execute the code style checks.

## Building the docker image

To build the docker image, execute the script `docker-build.sh`in the top level directory. By default the created image is tagged as latest. This script accepts the version tag
as an argument. For example, `./docker-build.sh 1.0.0` will create the docker image `codeveros\ui:1.0.0` This file performs all node and npm commands as part of the Docker build,
and does not require any setup or building of files (nor does it require node to be installed) prior to executing this script. This file uses the `Dockerfile.multistage` 
dockerfile.

Alternatively, you may choose to build the docker image using the pre-built dist files following the build step above. The default `Dockerfile` simply copies the contents of the
dist folder into the built image. This file is mainly used as part of the CI pipeline which requires building and testing the application prior to building the docker image.
